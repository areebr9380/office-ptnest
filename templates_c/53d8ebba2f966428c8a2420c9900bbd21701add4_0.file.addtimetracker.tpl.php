<?php
/* Smarty version 3.1.29, created on 2020-02-05 11:53:17
  from "/home/ptnest/public_html/office/collab/templates/standard/forms/addtimetracker.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e3a665d6bb150_31668937',
  'file_dependency' => 
  array (
    '53d8ebba2f966428c8a2420c9900bbd21701add4' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/forms/addtimetracker.tpl',
      1 => 1495706442,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3a665d6bb150_31668937 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/home/ptnest/public_html/office/collab/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
echo '<script'; ?>
 type="text/javascript" src="include/js/timetracker_widget.js"><?php echo '</script'; ?>
>
<div class="block_in_wrapper">

	<form class="main" id="trackeradd" method="post" action="managetimetracker.php?action=add"">
		<fieldset>

			<input type="hidden" name="project" value="<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
" />

		 	<div class="row">
		  		<label for="ttday"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'startday');?>
:</label>
		  		<input type="text" class="text" style="width:80px;margin:0 6px 0 0;" id="ttday" name="ttday" required />
			</div>

			<div class="datepick">
				<div id="datepicker_addtt" class="picker display-none"></div>
			</div>

			<?php echo '<script'; ?>
 type="text/javascript">
				theCal = new calendar(<?php echo $_smarty_tpl->tpl_vars['theM']->value;?>
,<?php echo $_smarty_tpl->tpl_vars['theY']->value;?>
);
				theCal.dayNames = ["<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'monday');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'tuesday');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'wednesday');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'thursday');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'friday');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'saturday');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'sunday');?>
"];
				theCal.monthNames = ["<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'january');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'february');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'march');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'april');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'may');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'june');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'july');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'august');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'september');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'october');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'november');?>
","<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'december');?>
"];
				theCal.relateTo = "ttday";
				theCal.keepEmpty = false;
				theCal.dateFormat = "<?php echo $_smarty_tpl->tpl_vars['settings']->value['dateformat'];?>
";
				theCal.getDatepicker("datepicker_addtt");
			<?php echo '</script'; ?>
>

		  	<div class="row">
		  		<label for="started"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'started');?>
:</label>
		  		<input type="text" class="text" style="width:80px;margin:0 6px 0 0;" id="started" name="started" onkeyup=" populateHours();" required  pattern="^([01]?\d|2[0123]):[012345]\d$" value="08:00" />
			</div>
			<div class="row">
				<label for = "ended"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'ended');?>
:</label>
		  		<input type="text" class="text" style="width:80px;margin:0 6px 0 0;" id="ended" name="ended" onkeyup = " populateHours();" required pattern="^([01]?\d|2[0123]):[012345]\d$" value="09:00" />
			</div>

<!-- Requires rework to enable submitting times <1 hour
			<div class = "row">
				<label for = "workhours" >Hours:</label>
		  		<input type = "number" id = "workhours" name = "workhours" value = "1" min = "1" max = "10" step = "1" onkeyup = "populateEndtime();" onchange = "populateEndtime();" style="width:40px;"/>
			</div>
-->

			<input type="hidden" name="project" value="<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
" />

		  	<div class="row">
		  		<label for="trackcomm"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'comment');?>
:</label>
		  		<textarea name="comment" id="trackcomm"></textarea>
		  	</div>

		  	<div class="clear_both_b"></div>

		  	<div class="row">
				<label for="ttask"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'task');?>
:</label>
				<select name="ttask" id="ttask">
				  	<option value="0"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'chooseone');?>
</option>
				  	<?php
$__section_task_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_task']) ? $_smarty_tpl->tpl_vars['__smarty_section_task'] : false;
$__section_task_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['ptasks']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_task_0_total = $__section_task_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_task'] = new Smarty_Variable(array());
if ($__section_task_0_total != 0) {
for ($__section_task_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_task']->value['index'] = 0; $__section_task_0_iteration <= $__section_task_0_total; $__section_task_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_task']->value['index']++){
?>

				  		<?php if ($_smarty_tpl->tpl_vars['ptasks']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_task']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_task']->value['index'] : null)]['title'] != '') {?>
				  		<option value="<?php echo $_smarty_tpl->tpl_vars['ptasks']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_task']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_task']->value['index'] : null)]['ID'];?>
"><?php echo $_smarty_tpl->tpl_vars['ptasks']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_task']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_task']->value['index'] : null)]['title'];?>
</option>
				  		<?php } else { ?>
				  		<option value="<?php echo $_smarty_tpl->tpl_vars['ptasks']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_task']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_task']->value['index'] : null)]['ID'];?>
"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['ptasks']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_task']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_task']->value['index'] : null)]['text'],30,"...",true);?>
</option>
						<?php }?>

					<?php
}
}
if ($__section_task_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_task'] = $__section_task_0_saved;
}
?>
			  	</select>
		  	</div>

			<div class="row-butn-bottom">
				<label>&nbsp;</label>
				<button type="submit" onfocus="this.blur();"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'addbutton');?>
</button>
			</div>

		</fieldset>
	</form>

</div> 
<?php }
}
