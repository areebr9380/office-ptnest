<?php
/* Smarty version 3.1.29, created on 2019-05-14 12:04:04
  from "/home/ptnest/public_html/office/collab/templates/standard/tabsmenue-admin.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cda68647c17c6_97870276',
  'file_dependency' => 
  array (
    '58ee2a2a32be86965d2d742ba717e7716396f154' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/tabsmenue-admin.tpl',
      1 => 1414602512,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cda68647c17c6_97870276 ($_smarty_tpl) {
?>
<div class="tabswrapper">
	<ul class="tabs">
		<li class="projects"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['projecttab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="admin.php?action=projects"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'projectadministration');?>
</span></a></li>
		<li class="customers"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['customertab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="admin.php?action=customers"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'customeradministration');?>
</span></a></li>
		<li class="user"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['usertab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="admin.php?action=users"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'useradministration');?>
</span></a></li>
		<li class="system-settings"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['settingstab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="admin.php?action=system"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'systemadministration');?>
</span></a></li>
	</ul>
</div><?php }
}
