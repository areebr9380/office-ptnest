<?php
/* Smarty version 3.1.29, created on 2020-02-05 11:53:17
  from "/home/ptnest/public_html/office/collab/templates/standard/project.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e3a665d448328_53793202',
  'file_dependency' => 
  array (
    'e7db0cfd8e43fcac753f52836c0ae7218993bcfd' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/project.tpl',
      1 => 1504077218,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:tabsmenue-project.tpl' => 1,
    'file:editform.tpl' => 1,
    'file:calendar.tpl' => 1,
    'file:forms/addtimetracker.tpl' => 1,
    'file:log.tpl' => 1,
    'file:sidebar-a.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5e3a665d448328_53793202 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/home/ptnest/public_html/office/collab/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('jsload'=>"ajax",'stage'=>"project",'treeView'=>"treeView",'jsload1'=>"tinymce"), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tabsmenue-project.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('projecttab'=>"active"), 0, false);
?>


<div id="content-left">
    <div id="content-left-in">
        <div class="projects">

            <div class="infowin_left display-none"
                 id="timetrackerSystemMessage"
                 data-icon="templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/timetracker.png"
                 data-text-added="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'timetrackeradded');?>
">
            </div>

            <h1><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['project']->value['name'],45,"...",true);?>
<span>/ <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'overview');?>
</span></h1>

            <div class="statuswrapper">
                <ul>
                    <?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['projects']['close']) {?>
                        <?php if ($_smarty_tpl->tpl_vars['project']->value['status'] == 1) {?>
                            <li class="link" id="closetoggle">
                                <a class="close"
                                   href="javascript:closeElement('closetoggle','manageproject.php?action=close&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
');"
                                   title="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'close');?>
"></a>
                            </li>
                        <?php } else { ?>
                            <li class="link" id="closetoggle">
                                <a class="closed"
                                   href="manageproject.php?action=open&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"
                                   title="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'open');?>
"></a>
                            </li>
                        <?php }?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['projects']['edit']) {?>
                        <li class="link">
                            <a class="edit" href="javascript:void(0);" id="edit_butn"
                               onclick="blindtoggle('form_edit');toggleClass(this,'edit-active','edit');toggleClass('sm_project','smooth','nosmooth');toggleClass('sm_project_desc','smooth','nosmooth');"
                               title="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'edit');?>
"></a>
                        </li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['project']->value['desc']) {?>
                        <li class="link" onclick="blindtoggle('descript');toggleClass('desctoggle','desc_active','desc');">
                            <a class="desc" id="desctoggle"
                               href="#"
                               title="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'open');?>
"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'description');?>
</a>
                        </li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['projects']['del']) {
if ($_smarty_tpl->tpl_vars['project']->value['budget']) {?>
                        <li>
                            <a><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'budget');?>
: <?php echo $_smarty_tpl->tpl_vars['project']->value['budget'];?>
</a>
                        </li>
                    <?php }
}?>
                    <?php if ($_smarty_tpl->tpl_vars['project']->value['customer']['company'] != '') {?>
                        <li class="link" onclick="blindtoggle('customer');toggleClass('custtogle','desc_active','desc');">
                            <a class="desc" id="custtogle"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'customer');?>
: <?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['company'];?>
</a>
                        </li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['project']->value['daysleft'] != '' || $_smarty_tpl->tpl_vars['project']->value['daysleft'] == "0") {?>
                        <li <?php if ($_smarty_tpl->tpl_vars['project']->value['daysleft'] < 0) {?>class="red" <?php } elseif ($_smarty_tpl->tpl_vars['project']->value['daysleft'] == "0") {?>class="green"<?php }?>>
                            <a><?php echo $_smarty_tpl->tpl_vars['project']->value['daysleft'];?>
 <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'daysleft');?>
</a></li>
                    <?php }?>
                </ul>

                <div class="status">
                    <?php echo $_smarty_tpl->tpl_vars['done']->value;?>
%
                    <div class="statusbar">
                        <div class="complete" id="completed" style="width:<?php echo $_smarty_tpl->tpl_vars['done']->value;?>
%;"></div>
                    </div>
                </div>
            </div>

            
            <?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['projects']['edit']) {?>
                <div id="form_edit" class="addmenue display-none clear_both">
                    <div class="content-spacer"></div>
                    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:editform.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('showhtml'=>"no"), 0, false);
?>

                </div>
            <?php }?>

            <div class="nosmooth" id="sm_project_customer">
                <div id="customer" class="descript display-none">
                    <div class="content-spacer"></div>
                    <h2><?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['company'];?>
</h2>
                    <b><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'contactperson');?>
:</b> <?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['contact'];?>

                    <br/>
                    <b><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'email');?>
:</b> <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['email'];?>
</a>
                    <br/>
                    <b><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'phone');?>
 / <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'cellphone');?>
:</b> <?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['phone'];?>
 / <?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['mobile'];?>

                    <br/>
                    <b><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'url');?>
:</b> <a href="<?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['url'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['url'];?>
</a>
                    <br/><br/>
                    <b><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'address');?>
:</b><br/>
                    <?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['address'];?>

                    <br/><?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['zip'];?>
 <?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['city'];?>

                    <br/><?php echo $_smarty_tpl->tpl_vars['project']->value['customer']['country'];?>
<br/>
                </div>
            </div>

            <div class="nosmooth" id="sm_project_desc">
                <div id="descript" class="descript display-none">
                    <div class="content-spacer"></div>
                    <?php echo $_smarty_tpl->tpl_vars['project']->value['desc'];?>

                </div>
            </div>
        </div> 

        <div class="content-spacer"></div>
        <div class="nosmooth" id="sm_project">
            <div id="block_dashboard" class="block">

                
                <?php if ($_smarty_tpl->tpl_vars['tree']->value[0][0] > 0) {?>
                    <div class="projects dtree padding-bottom-two-px">
                        <div class="headline accordion_toggle">
                            <a href="javascript:void(0);" id="treehead_toggle" class="win_block" onclick=""></a>

                            <h2>
                                <img src="./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/projects.png"
                                     alt=""/><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'projecttree');?>

                            </h2>
                        </div>

                        <div class="block accordion_content overflow-hidden display-none" id="treehead">
                            <div class="block_in_wrapper" style="padding-top:0px;">
                                <?php echo '<script'; ?>
 type="text/javascript">
                                    var projectTree = new dTree('projectTree');
                                    projectTree.config.useCookies = true;
                                    projectTree.config.useSelection = false;
                                    projectTree.add(0, -1, '');
                                    // Milestones
                                    <?php
$__section_titem_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_titem']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem'] : false;
$__section_titem_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['tree']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_titem_0_total = $__section_titem_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_titem'] = new Smarty_Variable(array());
if ($__section_titem_0_total != 0) {
for ($__section_titem_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] = 0; $__section_titem_0_iteration <= $__section_titem_0_total; $__section_titem_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']++){
?>
                                    projectTree.add("m" +<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['ID'];?>
, 0, "<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['name'];?>
", "managemilestone.php?action=showmilestone&msid=<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['ID'];?>
&id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
", "", "", "templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/miles.png", "templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/miles.png", "", <?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['daysleft'];?>
);

                                    // Task lists
                                    <?php
$__section_tlist_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist'] : false;
$__section_tlist_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists']) ? count($_loop) : max(0, (int) $_loop));
$__section_tlist_1_total = $__section_tlist_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_tlist'] = new Smarty_Variable(array());
if ($__section_tlist_1_total != 0) {
for ($__section_tlist_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] = 0; $__section_tlist_1_iteration <= $__section_tlist_1_total; $__section_tlist_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']++){
?>
                                    projectTree.add("tl" +<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['ID'];?>
, "m" +<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['milestone'];?>
, "<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['name'];?>
", "managetasklist.php?action=showtasklist&id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
&tlid=<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['ID'];?>
", "", "", "templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/tasklist.png", "templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/tasklist.png");

                                    // Tasks from lists
                                    <?php
$__section_ttask_2_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_ttask']) ? $_smarty_tpl->tpl_vars['__smarty_section_ttask'] : false;
$__section_ttask_2_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['tasks']) ? count($_loop) : max(0, (int) $_loop));
$__section_ttask_2_total = $__section_ttask_2_loop;
$_smarty_tpl->tpl_vars['__smarty_section_ttask'] = new Smarty_Variable(array());
if ($__section_ttask_2_total != 0) {
for ($__section_ttask_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index'] = 0; $__section_ttask_2_iteration <= $__section_ttask_2_total; $__section_ttask_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index']++){
?>
                                    projectTree.add("ta" +<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['tasks'][(isset($_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index'] : null)]['ID'];?>
, "tl" +<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['tasks'][(isset($_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index'] : null)]['liste'];?>
, "<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['tasks'][(isset($_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index'] : null)]['title'];?>
", "managetask.php?action=showtask&tid=<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['tasks'][(isset($_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index'] : null)]['ID'];?>
&id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
", "", "", "templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/task.png", "templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/task.png", "", <?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['tasklists'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tlist']->value['index'] : null)]['tasks'][(isset($_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ttask']->value['index'] : null)]['daysleft'];?>
);
                                    <?php
}
}
if ($__section_ttask_2_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_ttask'] = $__section_ttask_2_saved;
}
?>

                                    // End task lists
                                    <?php
}
}
if ($__section_tlist_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_tlist'] = $__section_tlist_1_saved;
}
?>
                                    // Messages
                                    <?php
$__section_tmsg_3_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_tmsg']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmsg'] : false;
$__section_tmsg_3_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['messages']) ? count($_loop) : max(0, (int) $_loop));
$__section_tmsg_3_total = $__section_tmsg_3_loop;
$_smarty_tpl->tpl_vars['__smarty_section_tmsg'] = new Smarty_Variable(array());
if ($__section_tmsg_3_total != 0) {
for ($__section_tmsg_3_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index'] = 0; $__section_tmsg_3_iteration <= $__section_tmsg_3_total; $__section_tmsg_3_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index']++){
?>
                                    <?php if ($_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['messages'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index'] : null)]['milestone'] > 0) {?>
                                    projectTree.add("msg" +<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['messages'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index'] : null)]['ID'];?>
, "m" +<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['messages'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index'] : null)]['milestone'];?>
, "<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['messages'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index'] : null)]['title'];?>
", "managemessage.php?action=showmessage&id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
&mid=<?php echo $_smarty_tpl->tpl_vars['tree']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_titem']->value['index'] : null)]['messages'][(isset($_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_tmsg']->value['index'] : null)]['ID'];?>
", "", "", "templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/msgs.png", "templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/msgs.png");
                                    <?php }?>
                                    <?php
}
}
if ($__section_tmsg_3_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_tmsg'] = $__section_tmsg_3_saved;
}
?>
                                    // End Messages
                                    <?php
}
}
if ($__section_titem_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_titem'] = $__section_titem_0_saved;
}
?>
                                    // End milestones
                                    document.write(projectTree);
                                <?php echo '</script'; ?>
>

                                <br/>

                                <form id="treecontrol" action="#">
                                    <fieldset>
                                        <div class="row-butn-bottom">
                                            <button type="reset" id="openall" onclick="projectTree.openAll();"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'openall');?>
</button>
                                            <button type="reset" id="closeall" onclick="projectTree.closeAll();"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'closeall');?>
</button>
                                        </div>
                                    </fieldset>
                                </form>

                            </div> 
                        </div> 
                    </div>
                <?php }?>
                
                
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('context'=>"project"), 0, false);
?>


                
                <?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['timetracker']['add']) {?>
                    <div class="timetrack padding-bottom-two-px">
                        <div class="headline accordion_toggle">
                            <a href="javascript:void(0);" id="trackerhead_toggle" class="win_none" onclick=""></a>

                            <h2>
                                <a href="managetimetracker.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
" title="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'timetracker');?>
"><img
                                            src="./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/timetracker.png"
                                            alt=""/><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'timetracker');?>
</a>
                            </h2>
                        </div>

                        <div class="block accordion_content overflow-hidden display-none" id="trackerhead">
                            <div id="trackerform" class="addmenue">
                                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:forms/addtimetracker.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            </div>
                            <div class="tablemenue"></div>
                        </div> 
                    </div>
                    

                    <!--<div class="content-spacer"></div>-->
                <?php }?>
                

                <div class="neutral padding-bottom-two-px">
                    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:log.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
            </div> 
            
        </div>

        
        <?php echo '<script'; ?>
 type="text/javascript" src="include/js/accordion.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="include/js/modal.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="include/js/views/project.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript">
            //changeshow('manageproject.php?action=cal&id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
','thecal','progress');
            projectCalendar.url = projectCalendar.url + "&id=" + <?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
;

            var calendarView = createView(projectCalendar);
            calendarView.afterLoad(function () {
                var theBlocks = document.querySelectorAll("#block_dashboard > div .headline > a");

                //loop through the blocks and add the accordion toggle link
                var openSlide = 0;
                for (i = 0; i < theBlocks.length; i++) {
                    var theAction = theBlocks[i].getAttribute("onclick");
                    theAction += "activateAccordeon(" + i + ");";
                    theBlocks[i].setAttribute("onclick", theAction);
                }
                activateAccordeon(0);
            });

            //get the form to be submitted
            var addProjectForm = document.getElementById("trackeradd");
            //assign the view to be updated after submitting to the formView variable
            var formView = calendarView;
            formView.doUpdate = false;
            formView.itemType = "timetracker";
            //add an event listener capaturing the submit event of the form
            //add submitForm() as the handler for the event, and bind the form view to it
            addProjectForm.addEventListener("submit", submitForm.bind(formView));
        <?php echo '</script'; ?>
>
        


    </div> 
</div> 

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sidebar-a.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('showcloud'=>"1"), 0, false);
?>




<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
