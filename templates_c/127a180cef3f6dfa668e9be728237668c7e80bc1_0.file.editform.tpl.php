<?php
/* Smarty version 3.1.29, created on 2020-02-05 11:53:17
  from "/home/ptnest/public_html/office/collab/templates/standard/editform.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e3a665d627b88_72029061',
  'file_dependency' => 
  array (
    '127a180cef3f6dfa668e9be728237668c7e80bc1' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/editform.tpl',
      1 => 1474964494,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:tabsmenue-project.tpl' => 1,
    'file:sidebar-a.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5e3a665d627b88_72029061 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/home/ptnest/public_html/office/collab/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
if ($_smarty_tpl->tpl_vars['showhtml']->value != "no") {?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('jsload'=>"ajax",'jsload1'=>"tinymce"), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tabsmenue-project.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('projecttab'=>"active"), 0, false);
?>


<div id="content-left">
	<div id="content-left-in">
		<div class="projects">

			<div class="breadcrumb">
				<a href="manageproject.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['project']->value['name'];?>
"><img src="./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/projects.png" alt="" /><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['project']->value['name'],50,"...",true);?>
</a>
				<span>&nbsp;/...</span>
			</div>

			<h1 class="second">
				<img src="./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/projects.png" alt="" />
				<?php echo $_smarty_tpl->tpl_vars['project']->value['name'];?>

			</h1>

<?php }?>

<?php if ((($tmp = @$_smarty_tpl->tpl_vars['async']->value)===null||$tmp==='' ? '' : $tmp) == "yes") {?>

			
				<?php echo '<script'; ?>
 type="text/javascript">
					//	theme_advanced_statusbar_location : "bottom",
					tinyMCE.init({
						mode : "textareas",
						theme : "advanced",
						language: "<?php echo $_smarty_tpl->tpl_vars['locale']->value;?>
",
						width: "400px",
						height: "250px",
						plugins : "inlinepopups,style,advimage,advlink,xhtmlxtras,safari,template",
						theme_advanced_buttons1 : "bold,italic,underline,|,fontsizeselect,forecolor,|,bullist,numlist,|,link,unlink,image",
						theme_advanced_buttons2 : "",
						theme_advanced_buttons3 : "",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_path : false,
						extended_valid_elements : "a[name|href|target|title],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|name],font[face|size|color|style],span[class|align|style]",
					    theme_advanced_statusbar_location: "bottom",
					    theme_advanced_resizing : true,
						theme_advanced_resizing_use_cookie : false,
						theme_advanced_resizing_min_width : "400px",
						theme_advanced_resizing_max_width : "600px",
						theme_advanced_resize_horizontal : false,
						force_br_newlines : true,
						cleanup: true,
						cleanup_on_startup: true,
						force_p_newlines : false,
						convert_newlines_to_brs : false,
						forced_root_block : false,
						external_image_list_url: 'manageajax.php?action=jsonfiles&id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
'
					});
				<?php echo '</script'; ?>
>
			

<?php }?>

			<div class="block_in_wrapper">

				<h2><?php echo $_smarty_tpl->tpl_vars['langfile']->value['editproject'];?>
</h2>

				<form novalidate class="main" method="post" action="manageproject.php?action=edit&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
" onsubmit="return validateCompleteForm(this,'input_error');">
					<fieldset>

						<div class="row">
							<label for="name"><?php echo $_smarty_tpl->tpl_vars['langfile']->value['name'];?>
:</label>
							<input type="text" class="text" name="name" id="name" required="1" realname="<?php echo $_smarty_tpl->tpl_vars['langfile']->value['name'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['project']->value['name'];?>
" />
						</div>

						<div class="row">
							<label for="desc"><?php echo $_smarty_tpl->tpl_vars['langfile']->value['description'];?>
:</label>
							<div class="editor">
								<textarea name="desc" id="desc" rows="3" cols="1"><?php echo $_smarty_tpl->tpl_vars['project']->value['desc'];?>
</textarea>
							</div>
						</div>

						<div class="row">
							<label for="budget"><?php echo $_smarty_tpl->tpl_vars['langfile']->value['budget'];?>
:</label>
							<input type="text" class="text" name="budget" id="budget" realname="<?php echo $_smarty_tpl->tpl_vars['langfile']->value['budget'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['project']->value['budget'];?>
" />
						</div>

						<div class="row">
							<label for="end"><?php echo $_smarty_tpl->tpl_vars['langfile']->value['due'];?>
:</label>
							<input type="text" class="text" value="<?php echo $_smarty_tpl->tpl_vars['project']->value['endstring'];?>
" name="end" id="end" <?php if ($_smarty_tpl->tpl_vars['project']->value['end'] == 0) {?> disabled="disabled" <?php }?> realname="<?php echo $_smarty_tpl->tpl_vars['langfile']->value['due'];?>
" />
						</div>
						<div class="row">
							<label for="neverdue"></label>
							<input type="checkbox" class="checkbox" value="neverdue" name="neverdue" id="neverdue" <?php if ($_smarty_tpl->tpl_vars['project']->value['end'] == 0) {?> checked = "checked" <?php }?> onclick="$('end').value='';$('end').disabled=!$('end').disabled;">
							<label><?php echo $_smarty_tpl->tpl_vars['langfile']->value['neverdue'];?>
</label>
						</div>
                        <div class="row">
                            <label for="changeallduedates"></label>
                            <input type="checkbox" class="checkbox" name="changeallduedates" id="changeallduedates" />
                            <label><?php echo $_smarty_tpl->tpl_vars['langfile']->value['changeallduedates'];?>
</label>
                        </div>

						<div class="datepick">
							<div id="datepicker_project" class="picker display-none"></div>
						</div>

						<?php echo '<script'; ?>
 type="text/javascript">
							theCal<?php echo $_smarty_tpl->tpl_vars['lists']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_list']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_list']->value['index'] : null)]['ID'];?>
 = new calendar(<?php echo $_smarty_tpl->tpl_vars['theM']->value;?>
,<?php echo $_smarty_tpl->tpl_vars['theY']->value;?>
);
							theCal<?php echo $_smarty_tpl->tpl_vars['lists']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_list']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_list']->value['index'] : null)]['ID'];?>
.dayNames = ["<?php echo $_smarty_tpl->tpl_vars['langfile']->value['monday'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['tuesday'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['wednesday'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['thursday'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['friday'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['saturday'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['sunday'];?>
"];
							theCal<?php echo $_smarty_tpl->tpl_vars['lists']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_list']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_list']->value['index'] : null)]['ID'];?>
.monthNames = ["<?php echo $_smarty_tpl->tpl_vars['langfile']->value['january'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['february'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['march'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['april'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['may'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['june'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['july'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['august'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['september'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['october'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['november'];?>
","<?php echo $_smarty_tpl->tpl_vars['langfile']->value['december'];?>
"];
							theCal<?php echo $_smarty_tpl->tpl_vars['lists']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_list']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_list']->value['index'] : null)]['ID'];?>
.dateFormat = "<?php echo $_smarty_tpl->tpl_vars['settings']->value['dateformat'];?>
";
							theCal<?php echo $_smarty_tpl->tpl_vars['lists']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_list']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_list']->value['index'] : null)]['ID'];?>
.relateTo = "end";
							theCal<?php echo $_smarty_tpl->tpl_vars['lists']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_list']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_list']->value['index'] : null)]['ID'];?>
.getDatepicker("datepicker_project");
						<?php echo '</script'; ?>
>

						<div class="row-butn-bottom">
							<label>&nbsp;</label>
							<button type="submit" onfocus="this.blur();"><?php echo $_smarty_tpl->tpl_vars['langfile']->value['send'];?>
</button>
							<button type="button" onclick="<?php if ($_smarty_tpl->tpl_vars['projectov']->value == "no") {?>blindtoggle('form_edit'); toggleClass('edit_butn','edit-active','edit');toggleClass('sm_project','smooth','nosmooth');toggleClass('sm_project_desc','smooth','nosmooth');<?php } else { ?>;blindtoggle('form_addmyproject');<?php }?> return false;" onfocus="this.blur();" <?php if ($_smarty_tpl->tpl_vars['showhtml']->value != "no") {?> style="display:none;"<?php }?>><?php echo $_smarty_tpl->tpl_vars['langfile']->value['cancel'];?>
</button>
						</div>

					</fieldset>
				</form>

			</div> 

<?php if ($_smarty_tpl->tpl_vars['showhtml']->value != "no") {?>

			<div class="content-spacer"></div>

		</div> 
	</div> 
</div> 

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sidebar-a.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }
}
}
