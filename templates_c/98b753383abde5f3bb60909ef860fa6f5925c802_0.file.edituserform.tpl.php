<?php
/* Smarty version 3.1.29, created on 2020-02-05 11:53:34
  from "/home/ptnest/public_html/office/collab/templates/standard/edituserform.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e3a666eb326f4_28346532',
  'file_dependency' => 
  array (
    '98b753383abde5f3bb60909ef860fa6f5925c802' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/edituserform.tpl',
      1 => 1474545004,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:tabsmenue-user.tpl' => 1,
    'file:sidebar-a.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5e3a666eb326f4_28346532 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('jsload'=>"ajax"), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tabsmenue-user.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('edittab'=>"active"), 0, false);
?>


<div id="content-left">
	<div id="content-left-in">
		<div class="user">
			<h1><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'edituser');?>
<span>/ <?php echo $_smarty_tpl->tpl_vars['user']->value['name'];?>
</span></h1>
			<div class="userwrapper">
				<form novalidate class="main" method="post" action="manageuser.php?action=edit" enctype="multipart/form-data"  onsubmit="return validateCompleteForm(this,'input_error');"  >
					<fieldset>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="avatarcell" valign="top">
							
									<?php if ($_smarty_tpl->tpl_vars['user']->value['avatar'] != '') {?>
										<a href="#avatarbig" id="ausloeser">
											<div class="avatar-profile">
												<img src="thumb.php?pic=files/<?php echo $_smarty_tpl->tpl_vars['cl_config']->value;?>
/avatar/<?php echo $_smarty_tpl->tpl_vars['user']->value['avatar'];?>
&amp;width=122;" alt="" />
											</div>
										</a>
									<?php } else { ?>
										<?php if ($_smarty_tpl->tpl_vars['user']->value['gender'] == "f") {?>
											<div class="avatar-profile">
												<img src="thumb.php?pic=templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/no-avatar-female.jpg&amp;width=122;" alt="" />
											</div>
										<?php } else { ?>
											<div class="avatar-profile">
												<img src="thumb.php?pic=templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/no-avatar-male.jpg&amp;width=122;" alt="" />
											</div>
										<?php }?>
									<?php }?>
									
									<div id="avatarbig" style="display:none;">
										<a href="javascript:Control.Modal.close();">
											<img src="thumb.php?pic=files/<?php echo $_smarty_tpl->tpl_vars['cl_config']->value;?>
/avatar/<?php echo $_smarty_tpl->tpl_vars['user']->value['avatar'];?>
&amp;width=480&amp;height=480;" alt="" />
										</a>
									</div>
								</td>
								
								<td>
									<div class="message">
										<div class="block">
											
											<table cellpadding="0" cellspacing="0" border="0">
												
												<colgroup>
													<col class="a" />
													<col class="b" />
												</colgroup>
												
												<thead><tr><th colspan="2"></th></tr></thead>
												<tfoot><tr><td colspan="2"></td></tr></tfoot>
												
												<tbody class="color-a">
													<tr>
														<td><label for="name"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'user');?>
:</label></td>
														<td class="right"><input type="text" class="text" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['name'];?>
" name="name" id="name" required="1" realname="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'name');?>
" tabindex="1" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="avatar"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'avatar');?>
:</label></td>
														<td class="right">
															<div class="fileinput" >
																<input type="file" class="file" name="userfile" id="avatar" realname="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'file');?>
" size="19" onchange="file_avatar.value = this.value;" tabindex="2" />
																<table class="faux" cellpadding="0" cellspacing="0" border="0">
																	<tr>
																		<td><input type="text" class="text-file" name="file-avatar" id="file_avatar"></td>
																		<td class="choose"><button class="inner" onclick="return false;"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'chooseone');?>
</button></td>
																	</tr>
																</table>
															</div>
														</td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td></td>
														<td class="right"></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="company"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'company');?>
:</label></td>
														<td class="right">
															<input type="text" name="company" id="company" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['company'];?>
" />
														</td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td><label for="email"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'email');?>
:</label></td>
														<td class="right"><input type="text" class="text" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['email'];?>
" name="email" id="email"  regexp="EMAIL"  required="1" realname="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'email');?>
" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="web"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'url');?>
:</label></td>
														<td class="right"><input type="text" class="text" name="web" id="web" realname="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'url');?>
" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['url'];?>
" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td><label for="tel1"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'phone');?>
:</label></td>
														<td class="right"><input type="text" class="text" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['tel1'];?>
" name="tel1" id="tel1" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="tel2"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'cellphone');?>
:</label></td>
														<td class="right"><input type="text" class="text" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['tel2'];?>
" name="tel2" id="tel2" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td></td>
														<td class="right"></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="address1"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'address');?>
:</label></td>
														<td class="right"><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['adress'];?>
" name="address1" id="address1" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td><label for="zip"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'zip');?>
:</label></td>
														<td class="right"><input type="text" name="zip" id="zip" realname="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'zip');?>
" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['zip'];?>
" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="address2"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'city');?>
:</label></td>
														<td class="right"><input type="text" class="text" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['adress2'];?>
" name="address2" id="address2" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td><label for="country"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'country');?>
:</label></td>
														<td class="right"><input type="text" class="text" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['country'];?>
" name="country" id="country" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="state"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'state');?>
:</label></td>
														<td class="right"><input type="text" class="text" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['state'];?>
" name="state" id="state" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td></td>
														<td class="right"></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="gender"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'gender');?>
:</label></td>
														<td class="right">
															<select name="gender" id="gender" realname="<?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'gender');?>
" />
																<?php if ($_smarty_tpl->tpl_vars['user']->value['gender'] == '') {?>
																	<option value="" selected><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'chooseone');?>
</option>
																<?php }?>
																<option <?php if ($_smarty_tpl->tpl_vars['user']->value['gender'] == "m") {?> selected="selected" <?php }?> value="m"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'male');?>
</option>
																<option <?php if ($_smarty_tpl->tpl_vars['user']->value['gender'] == "f") {?> selected="selected" <?php }?> value="f"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'female');?>
</option>
															</select>
														</td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td><label for="locale"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'locale');?>
:</label></td>
														<td class="right">
															<select name="locale" id="locale">
																<option value="" <?php if ($_smarty_tpl->tpl_vars['user']->value['locale'] == '') {?> selected="selected" <?php }?> ><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'systemdefault');?>
</option>
																<?php
$__section_lang_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_lang']) ? $_smarty_tpl->tpl_vars['__smarty_section_lang'] : false;
$__section_lang_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['languages_fin']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_lang_0_total = $__section_lang_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_lang'] = new Smarty_Variable(array());
if ($__section_lang_0_total != 0) {
for ($__section_lang_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_lang']->value['index'] = 0; $__section_lang_0_iteration <= $__section_lang_0_total; $__section_lang_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_lang']->value['index']++){
?>
																	<option value="<?php echo $_smarty_tpl->tpl_vars['languages_fin']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lang']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lang']->value['index'] : null)]['val'];?>
" <?php if ($_smarty_tpl->tpl_vars['languages_fin']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lang']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lang']->value['index'] : null)]['val'] == $_smarty_tpl->tpl_vars['user']->value['locale']) {?> selected="selected" <?php }?> ><?php echo $_smarty_tpl->tpl_vars['languages_fin']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_lang']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_lang']->value['index'] : null)]['str'];?>
</option>
																<?php
}
}
if ($__section_lang_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_lang'] = $__section_lang_0_saved;
}
?>
															</select>
														</td>
													</tr>
												</tbody>
												
												<input type="hidden" name="admin" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['user']->value['admin'])===null||$tmp==='' ? '' : $tmp);?>
" />
												
												<tbody class="color-b">
													<tr>
														<td><label for="oldpass"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'oldpass');?>
:</label></td>
														<td class="right"><input type="password" class="text" name="oldpass" id="oldpass" autocomplete="off" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td><label for="newpass"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'newpass');?>
:</label></td>
														<td class="right"><input type="password" name="newpass" id="newpass" autocomplete="off" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-b">
													<tr>
														<td><label for="repeatpass"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'repeatpass');?>
:</label></td>
														<td class="right"><input type="password" name="repeatpass" id="repeatpass" autocomplete="off" /></td>
													</tr>
												</tbody>
												
												<tbody class="color-a">
													<tr>
														<td></td>
														<td class="right">
															<button type="submit" onfocus="this.blur()"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'send');?>
</button>
														</td>
													</tr>
												</tbody>
												
											</table>
											
										</div> 
									</div> 
									
								</td>
							</tr>
							
						</table>
						
					</fieldset>
				</form>
				
				
					<?php echo '<script'; ?>
 type="text/javascript">
						new Control.Modal('ausloeser',{
						opacity: 0.8,
						position: 'absolute',
						width: 480,
						height: 480,
						fade:true,
						containerClassName: 'pics',
						overlayClassName: 'useroverlay'
						});
					<?php echo '</script'; ?>
>
				
				
			</div> 
			
			<div class="content-spacer"></div>
			
		</div> 
	</div> 
</div> 

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sidebar-a.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
