<?php
/* Smarty version 3.1.29, created on 2020-02-05 11:53:17
  from "/home/ptnest/public_html/office/collab/templates/standard/calendar.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e3a665d6927c2_09401229',
  'file_dependency' => 
  array (
    'fc51e728e12a9bcb4d068f860df326549a9781c5' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/calendar.tpl',
      1 => 1495631040,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3a665d6927c2_09401229 ($_smarty_tpl) {
?>
<div id="desktopCalendar" class="miles padding-bottom-two-px"  v-cloak>
    <div class="headline">
        <a href="javascript:void(0);" id="mileshead_toggle" class="win_none" onclick=""></a>

        <div class="wintools">
            <loader block="desktopCalendar" loader="loader-calendar.gif"></loader>
        </div>

        <h2>
            <img src="./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/miles.png" alt=""/><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'calendar');?>


        </h2>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['context']->value == "project") {?>
    <div class="block accordion_content overflow-hidden" id="mileshead" >
        <?php } else { ?>
        <div class="block blockaccordion_content overflow-hidden" id="mileshead">
            <?php }?>
            <div class="bigcal">
                <table cellpadding="0" cellspacing="1" border="0" class="thecal">
                    
                    <!--Calender head area -->
                    <thead class="calhead">
                    <tr>
                        <th>
                            <a class="scroll_left"
                               v-bind:href="'javascript:updateCalendar(calendarView,\''+items.previousMonth+'\',\''+items.previousYear+'\');'"></a>
                        </th>
                        <th colspan="5" align="center">
                            <!--Localized month & year -->
                            {{items.monthName}} {{items.selectedYear}}
                        </th>
                        <th>
                            <a class="scroll_right"
                               v-bind:href="'javascript:updateCalendar(calendarView,\''+items.nextMonth+'\',\''+items.nextYear+'\');'"></a>
                        </th>
                    </tr>

                    
                    <!--Localized days -->
                    <tr class="dayhead">
                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['monday'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['tuesday'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['wednesday'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['thursday'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['friday'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['saturday'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['sunday'];?>
</th>
                    </tr>
                    </thead>

                    <tbody class="content">
                    
                    <tr v-for="week in items.weeks" valign="top">
                        <!--Iterate days of current week -->
                        <td v-for="day in week"
                            v-bind:class="{
                           'today': day.currmonth == 1 && items.currentMonth == items.selectedMonth && items.currentYear == items.selectedYear && items.currentDay == day.val,
                           'second': items.currentDay != day.val,
                           'othermonth': day.currmonth != 1
                           }"
                            v-bind:id="day.val">
                            {{day.val}}

                            <!--Only output tasks/milestones if the day belongs to the current month -->
                            <div v-if="day.currmonth == 1" class="calcontent">
                                <!--Milestones -->
                                <template v-if="day.milesnum > 0">
                                    <a v-bind:href="'javascript:openModal(\'miles_modal'+day.val+'\');'">
                                        <img src="templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/miles.png"
                                             alt=""/>
                                    </a>

                                    <div v-bind:id="'miles_modal'+day.val" class="milesmodal display-none" >
                                        <div class="modaltitle">
                                            <img src="./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/miles.png"
                                                 alt=""/>
                                            <?php echo $_smarty_tpl->tpl_vars['langfile']->value['milestones'];?>

                                            {{day.val}}.{{items.currentMonth}}.{{items.currentYear}}
                                            <a class="winclose"
                                               v-bind:href="'javascript:closeModal(\'miles_modal'+day.val+'\');'"></a>
                                        </div>

                                        <div class="inmodal">
                                            <div class="miles">
                                                <div class="block">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        
                                                        <thead>
                                                        <?php if ($_smarty_tpl->tpl_vars['context']->value != "project") {?><th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['project'];?>
</th><?php }?>
                                                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['milestone'];?>
</th>
                                                        <th class="tools"><?php echo $_smarty_tpl->tpl_vars['langfile']->value['daysleft'];?>
</th>
                                                        </thead>
                                                        

                                                        <tbody v-for="milestone in day.milestones" class="alternateColors">
                                                        <tr>
                                                            
                                                            <?php if ($_smarty_tpl->tpl_vars['context']->value != "project") {?>
                                                            <td>{{{milestone.pname}}}</td>
                                                            <?php }?>
                                                            <td>
                                                                <a v-bind:href="'managemilestone.php?action=showmilestone&amp;msid='+milestone.ID+'&amp;id='+milestone.project"
                                                                   :title="milestone.title">{{{milestone.name}}}</a>
                                                            </td>
                                                            <td class="tools">
                                                                {{milestone.daysleft}}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                                <!--Milestones End -->


                                <!--Tasks -->
                                <template v-if="day.tasksnum > 0">
                                    <a v-bind:href="'javascript:openModal(\'tasks_modal'+day.val+'\');'">
                                        <img src="templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/task.png"
                                             alt=""/>
                                    </a>

                                    <div v-bind:id="'tasks_modal' + day.val" class="tasksmodal display-none">
                                        <div class="modaltitle">
                                            <img src="./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/tasklist.png"
                                                 alt=""/>
                                            <?php echo $_smarty_tpl->tpl_vars['langfile']->value['tasklist'];?>

                                            
                                            {{day.val}}.{{items.currentMonth}}.{{items.currentYear}}
                                            <a class="winclose"
                                               v-bind:href="'javascript:closeModal(\'tasks_modal'+day.val+'\');'"></a>
                                        </div>
                                        <div class="inmodal">
                                            <div class="tasks">
                                                <div class="block">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        
                                                        <thead>
                                                        <?php if ($_smarty_tpl->tpl_vars['context']->value != "project") {?><th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['project'];?>
</th><?php }?>
                                                        <th><?php echo $_smarty_tpl->tpl_vars['langfile']->value['task'];?>
</th>
                                                        <th class="tools"><?php echo $_smarty_tpl->tpl_vars['langfile']->value['daysleft'];?>
</th>
                                                        </thead>
                                                        
                                                        <tbody v-for="task in day.tasks" class="alternateColors">
                                                        <tr>
                                                            
                                                            <?php if ($_smarty_tpl->tpl_vars['context']->value != "project") {?>
                                                            <td>{{{task.pname}}}</td>
                                                            <?php }?>
                                                            <td>
                                                                <a v-bind:href="'managetask.php?action=showtask&amp;tid='+task.ID+'&amp;id='+task.project"
                                                                   v-bind:title="task.title">
                                                                    {{{task.title}}}
                                                                </a>

                                                            </td>
                                                            <td class="tools">{{task.daysleft}}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                                <!-- tasks end -->
                            </div>
                        </td>
                    </tr>
                     <!--Week End -->
                    </tbody>
                </table>
            </div>
            <!-- bigcal end -->
        </div>
        <!-- block END -->
    </div>
    <!-- miles END -->

<?php }
}
