<?php
/* Smarty version 3.1.29, created on 2020-02-05 11:53:17
  from "/home/ptnest/public_html/office/collab/templates/standard/tabsmenue-project.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e3a665d5b88d4_73394278',
  'file_dependency' => 
  array (
    '53412228af9792aaab168b7ec15d01199f2e4099' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/tabsmenue-project.tpl',
      1 => 1414602512,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3a665d5b88d4_73394278 ($_smarty_tpl) {
?>

<div class="tabswrapper">
	<ul class="tabs">
		<li class="projects"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['projecttab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="manageproject.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'project');?>
</span></a></li>

	<?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['milestones']['view']) {?>
		<li class="miles"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['milestab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="managemilestone.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'milestones');?>
</span></a></li>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['tasks']['view']) {?>
		<li class="tasks"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['taskstab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="managetask.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'tasklists');?>
</span></a></li>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['messages']['view']) {?>
		<li class="msgs"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['msgstab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="managemessage.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'messages');?>
</span></a></li>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['files']['view']) {?>
		<li class="files"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['filestab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="managefile.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'files');?>
</span></a></li>
	<?php }?>

		<li class="user"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['userstab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="manageuser.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'user');?>
</span></a></li>
	<?php if ($_smarty_tpl->tpl_vars['userpermissions']->value['timetracker']['view']) {?>
		<li class="timetrack"><a <?php if ((($tmp = @$_smarty_tpl->tpl_vars['timetab']->value)===null||$tmp==='' ? '' : $tmp) == "active") {?>class="active"<?php }?> href="managetimetracker.php?action=showproject&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'timetracker');?>
</span></a></li>
	<?php }?>
	</ul>
</div><?php }
}
