<?php
/* Smarty version 3.1.29, created on 2019-05-14 12:03:52
  from "/home/ptnest/public_html/office/collab/templates/standard/admindeluserform.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cda68582d5a21_44034710',
  'file_dependency' => 
  array (
    '541108fce88be7a4c4174dbbb55eea4331f25d24' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/admindeluserform.tpl',
      1 => 1414602512,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:tabsmenue-admin.tpl' => 1,
    'file:sidebar-a.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5cda68582d5a21_44034710 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('jsload'=>"ajax"), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:tabsmenue-admin.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('usertab'=>"active"), 0, false);
?>


<div id="content-left">
	<div id="content-left-in">
		<div class="user">
		
			<h1><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'deleteuser');?>
<span>/ <?php echo $_smarty_tpl->tpl_vars['user']->value['name'];?>
</span></h1>

			<div class="block_in_wrapper">
				
				<h2><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'deleteform');?>
</h2>
					
				<form class="main" method="post" action="admin.php?action=deleteuser&amp;id=<?php echo $_smarty_tpl->tpl_vars['user']->value['ID'];?>
" enctype="multipart/form-data"  onsubmit="return validateCompleteForm(this);"  >
					<fieldset>
						
						<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['ID'];?>
" />
						
						<?php
$__section_proj_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_proj']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj'] : false;
$__section_proj_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['projects']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_proj_0_total = $__section_proj_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_proj'] = new Smarty_Variable(array());
if ($__section_proj_0_total != 0) {
for ($__section_proj_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] = 0; $__section_proj_0_iteration <= $__section_proj_0_total; $__section_proj_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']++){
?>
							
							<div class="row">
								<h3><?php echo $_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['name'];?>
</h3>
							</div>
	                        
	                        <div class="row">
	                            <label for="<?php echo $_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['ID'];?>
membs"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'assignto');?>
:</label>
								<select name="uprojects[]" id="<?php echo $_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['ID'];?>
membs">
									<option value="<?php echo $_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['ID'];?>
#0" selected><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'deletetasks');?>
</option>
		                            <?php
$__section_member_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_member']) ? $_smarty_tpl->tpl_vars['__smarty_section_member'] : false;
$__section_member_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['members']) ? count($_loop) : max(0, (int) $_loop));
$__section_member_1_total = $__section_member_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_member'] = new Smarty_Variable(array());
if ($__section_member_1_total != 0) {
for ($__section_member_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_member']->value['index'] = 0; $__section_member_1_iteration <= $__section_member_1_total; $__section_member_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_member']->value['index']++){
?>
			                            
			                            <?php if ($_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['members'][(isset($_smarty_tpl->tpl_vars['__smarty_section_member']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_member']->value['index'] : null)]['ID'] != $_smarty_tpl->tpl_vars['user']->value['ID']) {?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['ID'];?>
#<?php echo $_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['members'][(isset($_smarty_tpl->tpl_vars['__smarty_section_member']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_member']->value['index'] : null)]['ID'];?>
" /><?php echo $_smarty_tpl->tpl_vars['projects']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_proj']->value['index'] : null)]['members'][(isset($_smarty_tpl->tpl_vars['__smarty_section_member']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_member']->value['index'] : null)]['name'];?>
</option>
			                            <?php }?>
			                     	
		                            <?php
}
}
if ($__section_member_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_member'] = $__section_member_1_saved;
}
?>
	                             </select>
							</div>
							
							<div class="content-spacer"></div>
                        
                        <?php
}
}
if ($__section_proj_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_proj'] = $__section_proj_0_saved;
}
?>
						
                        <div class="row-butn-bottom">
                        	<label>&nbsp;</label>
	                        <button type="submit"onfocus="this.blur();"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'send');?>
</button>
	                        <button onclick="javascript:history.back();return false;" onfocus="this.blur();"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'cancel');?>
</button>
                        </div>
					
					</fieldset>
				</form>
				
				<div class="clear_both"></div> 
			</div> 

			<div class="content-spacer"></div>
		</div> 
	</div> 
</div> 

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sidebar-a.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
