<?php
/* Smarty version 3.1.29, created on 2020-02-05 11:53:17
  from "/home/ptnest/public_html/office/collab/templates/standard/log.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e3a665d6fa991_27137737',
  'file_dependency' => 
  array (
    '2cdec81e8b33c141e9992945af33c88c0d4d8e3f' => 
    array (
      0 => '/home/ptnest/public_html/office/collab/templates/standard/log.tpl',
      1 => 1495631262,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e3a665d6fa991_27137737 ($_smarty_tpl) {
?>
<div class="headline">
    <a href="javascript:void(0);" id="loghead_toggle" class="win_none" onclick=""></a>

    <div class="wintools">

        <div class="export-main">

            <div class="progress display-none" id="progressprojectLog">
                <img src="templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/loader-neutral.gif"/>
            </div>
            <a class="export"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'export');?>
</span></a>

            <div class="export-in" style="width:46px;left: -46px;"> 
                <a class="pdf" href="manageproject.php?action=projectlogpdf&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'pdfexport');?>
</span></a>
                <a class="excel" href="manageproject.php?action=projectlogxls&amp;id=<?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
"><span><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'excelexport');?>
</span></a>
            </div>
        </div>

    </div>

    <h2>
        <img src="./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/activity.png" alt=""/><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'activity');?>

    </h2>
</div>

<div class="block accordion_content overflow-hidden display-none" id="loghead">
    <table id="projectLog" class="log" cellpadding="0" cellspacing="0" border="0"  v-cloak>
        <thead>
        <tr>
            <th class="a"></th>
            <th class="bc"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'action');?>
</th>
            <th class="d"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'user');?>
</th>
            <th class="tools"></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="5"></td>
        </tr>
        </tfoot>
        
        <tbody v-for="logitem in items" class="alternateColors"
               v-bind:id="'log_'+logitem.ID">
        <tr>
            <td style="padding:0" class="symbols">
                <img style="margin:0 0 0 3px;"
                     v-bind:src="'./templates/<?php echo $_smarty_tpl->tpl_vars['settings']->value['template'];?>
/theme/<?php echo $_smarty_tpl->tpl_vars['settings']->value['theme'];?>
/images/symbols/' + logitem.type + '.png'"
                     :alt="logitem.type" :title="logitem.type"/>
            </td>
            <td>
                <div class="toggle-in">
                    <strong>{{{logitem.name | truncate '35' }}}</strong><br/>
							<span class="info"><?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'was');?>

                                <span v-if="logitem.action == 1">
                                    
                                    <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'added');?>

                                    
                                </span>
								<span v-if="logitem.action == 2">
                                    
                                    <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'edited');?>

                                    
                                </span>
								<span v-if="logitem.action == 3">
                                    
                                    <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'deleted');?>

                                    
                                </span>
								<span v-if="logitem.action == 4">
                                    
                                    <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'opened');?>

                                    
                                </span>
						        <span v-if="logitem.action == 5">
                                    
                                    <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'closed');?>

                                    
                                </span>
						        <span v-if="logitem.action == 6">
                                    
                                    <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'assigned');?>

                                    
                                </span>
						        <span v-if="logitem.action == 7">
                                    
                                    <?php echo $_smarty_tpl->smarty->ext->configLoad->_getConfigVariable($_smarty_tpl, 'deassigned');?>

                                    
                                </span>
								{{logitem.datum}}
							</span>
                </div>
            </td>
            <td>
                <a v-bind:href="'manageuser.php?action=profile&amp;id='+logitem.user">{{logitem.username | truncate '30' }}</a>
            </td>
            <td class="tools"></td>
        </tr>
        </tbody>
        

        <tbody class="paging">
        <tr>
            <td></td>
            <td colspan="2">
                <div id="paging">
                    <pagination view="projectLogView" :pages="pages" :current-page="currentPage"></pagination>
                </div>
            </td>
            <td class="tools"></td>
        </tr>
        </tbody>

    </table>

    <div class="tablemenue"></div>
</div> 

<div class="content-spacer"></div>

<?php echo '<script'; ?>
 type="text/javascript">
    var projectLog = {
        el: "projectLog",
        itemType: "log",
        url: "manageproject.php?action=projectLog",
        dependencies: []
    }
    projectLog.url = projectLog.url + "&id=" + <?php echo $_smarty_tpl->tpl_vars['project']->value['ID'];?>
;

    pagination.itemsPerPage = 25;
    var projectLogView = createView(projectLog);
<?php echo '</script'; ?>
>
<?php }
}
